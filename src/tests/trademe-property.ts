
import { TestRun } from "../types";

export const TRADEME_PROPERTY_TEST: TestRun = {
  reportName: 'trademe-property',
  entryUrl: 'https://trademe.nz',
  entryWaitTime: 5000,
  steps: [
    {
      actionSelector: '.homepage__vertical-links-link--property',
      actionType: 'click',
      stepWaitTime: 10000
    // },
    // {
    //   actionSelector: '.o-button2--primary',
    //   actionType: 'click',
    //   stepWaitTime: 2000
    }
  ]
};