import { TestRun } from "../types";

export const TRADEME_MARKETPLACE_TEST: TestRun = {
  reportName: 'trademe-marketplace',
  entryUrl: 'https://trademe.nz',
  entryWaitTime: 5000,
  steps: [
    {
      actionSelector: '.homepage__vertical-links-link--marketplace',
      actionType: 'click',
      stepWaitTime: 2000
    }
  ]
};
