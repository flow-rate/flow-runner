import { TestRun } from "../types";
import { TRADEME_MARKETPLACE_TEST } from "./trademe-marketplace";
import { TRADEME_PROPERTY_TEST } from "./trademe-property";

export const TESTS: Array<TestRun> = [
  // TRADEME_PROPERTY_TEST,
  TRADEME_MARKETPLACE_TEST
];
