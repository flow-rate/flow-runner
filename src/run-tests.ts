import { TestRun, TestRunReport, Trace } from "./types";
import * as request from 'superagent';

const FLOW_METER_URL = 'https://yfkaw5lbkc.execute-api.ap-southeast-2.amazonaws.com/dev/flow-runner'
// const FLOW_METER_URL = 'http://localhost:3000/flow-runner'

export async function runTest (test: TestRun): Promise<{ report: TestRunReport, traces: Array<Trace> }> {
  const response = await request
    .post(FLOW_METER_URL)
    .set('accept', 'json')
    .send(test).catch(err => {
      console.log('request Error', err);
      throw err;
    });
  const { report, traces } = response.body;
  return Promise.resolve({ report, traces });
}