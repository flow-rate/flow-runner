#!/usr/bin/env node
import { readFileSync, writeFileSync } from 'fs';
import { join } from 'path';
import Handlebars from 'handlebars';

import { TESTS } from './tests';
import { runTest } from './run-tests';

const REPORT_PATH = 'reports';

(async () => {
  const htmlTemplate = readFileSync(join(__dirname, '../index.html')).toString();
  const template = Handlebars.compile(htmlTemplate);

  for await (const test of TESTS) {
    console.log('Running test - ', test.reportName);
    const { report, traces } = await runTest(test);
    writeFileSync(join(REPORT_PATH, `${test.reportName}.json`), JSON.stringify(report));
    
    for (const [index, stepReport] of report.stepReports.entries()) {
      const htmlReport = template(stepReport.flowReport);
      writeFileSync(join(REPORT_PATH, `${test.reportName}-step-${index}.html`), htmlReport);
    }

    for (const [index, trace] of traces.entries()) {
      writeFileSync(join(REPORT_PATH, `${test.reportName}-trace-step-${index}.json`), JSON.stringify(trace));
    }
  }
})();
